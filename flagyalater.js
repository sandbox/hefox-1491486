Drupal.behaviors.flagyalater = function(context) {
  // Unbind is used to prevent dblclick module from interfering.
  var flag_load_links = new Array();
  var flag_load_js = '';
  $('span.flagyalater:not(.flagyalater-processed)', context).addClass('flagyalater-processed').each(function() {
    var $item = $(this);
    var regrep = new RegExp(/flagyalater-flag-[A-Za-z_]*-[0-9]*/);
    var flagyalater_class = regrep.exec($item.attr('class'));
    flagyalater_class = flagyalater_class ? flagyalater_class[0] : null;
    if (flagyalater_class) {
      // If link already fetched, use it.
      if (flag_load_links[flagyalater_class]) {
        var $flag_item = $('.' + flagyalater_class + ':not(.flagyalater-loaded)');
        $flag_item.html(flag_load_links[flagyalater_class]);
        Drupal.attachBehaviors($flag_item.parent());
      }
      else {
        var flagyalater_class_split = flagyalater_class.split("-");
        if (flagyalater_class_split[3]) {
          flag_load_js = flag_load_js + 'flag_to_nids[' + flagyalater_class_split[2] + '][]=' + flagyalater_class_split[3] + '&';
        }
      }
    }
  });
  if (flag_load_js) {
    $.ajax({
      data: flag_load_js,
      url: Drupal.settings.flagyalater_path,
      type: 'GET',
      dataType: 'json',
      success: function(data) {
        $.each(data, function(key, val) {
          flag_load_links[key] = val;
          var $flag_item = $('.' + key + ':not(.flagyalater-loaded)');
          $flag_item.addClass('flagyalater-loaded').html(val);
          Drupal.attachBehaviors($flag_item.parent());
        });
        var flag_load_js = '';
      },
    });
  }
}
